name := "ADW4"

version := "0.1"

scalaVersion := "2.10.6"


val circeVersion = "0.7.0"
libraryDependencies ++= Seq(
  "io.circe"  %% "circe-core"     % circeVersion,
  "io.circe"  %% "circe-generic"  % circeVersion,
  "io.circe"  %% "circe-parser"   % circeVersion
)

val hipparchus_version = "1.5"
libraryDependencies ++= Seq(
  "org.hipparchus" % "hipparchus-stat" % hipparchus_version,
  "org.hipparchus" % "hipparchus-fft" % hipparchus_version,
  "org.hipparchus" % "hipparchus-geometry" % hipparchus_version,
  "org.hipparchus" % "hipparchus-optim" % hipparchus_version,
  "org.hipparchus" % "hipparchus-fitting" % hipparchus_version,
  "org.hipparchus" % "hipparchus-clustering" % hipparchus_version,
  "org.hipparchus" % "hipparchus-ode" % hipparchus_version,
  "org.hipparchus" % "hipparchus-samples" % hipparchus_version)


// Plotting
val jfree_version = "1.0.19"
//val jfree_version = "1.5.0" // TODO: depends on scalanlp/Breeze
val jfreesvg_version = "3.4"
libraryDependencies ++= Seq("org.jfree" % "jfreechart" % jfree_version,
  "org.jfree" % "jfreesvg" % jfreesvg_version)              // TODO: check version numbers


libraryDependencies ++=  Seq("com.rabbitmq" % "amqp-client" % "5.7.3")