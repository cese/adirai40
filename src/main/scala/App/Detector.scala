package App

import java.time.ZonedDateTime
import com.rabbitmq.client._
import DevelopedFunctions._
import App.Sender.Frm2Str

object Detector{

  // Default initializations
  var BffSz =80
  var FrmSz =64
  var NmbCpCoe = 5
  var Thrd1=0.93
  var Thrd2= 0.3
  var LFCCclss = LFCC(5100,NmbCpCoe,2550,10,NmbCpCoe,FrmSz)
  var AnomalyDetectorClss = CantelliFCCAnomalyDetector(NmbCpCoe*3,0.93,0.3)
  var Buffer = List[Array[Double]]()
  var counter=0
  var countLm=50

  //Connection to RabbitMQ
  def Connect(userName:String,password:String,hostName:String):Connection ={
    val factory = new ConnectionFactory();
    val userName= "inesc";
    val password= "adira_1n35c";
    val hostName= "194.117.30.45";
    val portNumber = 5672;
    factory.setUsername(userName)
    factory.setPassword(password)
    factory.setHost(hostName)
    factory.setPort(portNumber)
    val connection = factory.newConnection()
    connection
  }

  def Str2Frm(str:String):Array[Double]={
    val frm=str.split(",").map(_.toDouble)
    frm
  }

  // Process packet for anomaly presence
  def Process(frm: SensorRowData): Int = {

    //LFCC computation and concatenation
    var lfcc = LFCCclss.LFCCcomp(frm.X) ++ LFCCclss.LFCCcomp(frm.Y) ++ LFCCclss.LFCCcomp(frm.Z)

    if (Buffer.size < BffSz) {
      Buffer ++= List(lfcc)
      return 0
    }
    else{
      Buffer=Buffer.drop(1);  Buffer ++= List(lfcc)  // Update Buffer

      //Smoothing
      val smthLfcc = new Array[Double](NmbCpCoe*3)
      val tmp = new Array[Double](BffSz)
      for (k <- 0 until NmbCpCoe*3){
        for(j <- 0 until BffSz)
          tmp(j) = Buffer(j)(k)
        smthLfcc(k)=AnomalyDetectorClss.median(tmp)
      }

      // Anomaly detection(Prediction)
      val FD = AnomalyDetectorClss.predict(smthLfcc)


      // Train only with normal samples
      if (FD==0 || counter<countLm)
        AnomalyDetectorClss.train(smthLfcc)

      counter+=1

      FD
    }
  }

  // Get vector from JSON data
  def getFieldVector(Field:String,message:String): Array[Double] = {
    var idx1= message.indexOf(Field)
    var idx2= message.slice(idx1+Field.length,message.length).indexOf(",")
    var str = message.slice(idx1+Field.length+2,idx2+idx1+Field.length)
    var out = str.split(' ').map(_.toDouble)

    return out
  }

  // Get an int from JSON data
  def getFieldInt(Field:String,message:String): Int = {
    var idx1= message.indexOf(Field)
    var idx2= message.slice(idx1+Field.length,message.length).indexOf(",")
    var str = message.slice(idx1+Field.length+2,idx2+idx1+Field.length)

    return str.toInt
  }

  // Data packet format
  class SensorRowData{
    var timestamp:ZonedDateTime=_
    var machineId:String="_"
    var sensorId: String="_"
    var SeqID: Int= 0
    var X: Array[Double]= new Array[Double](FrmSz)
    var Y: Array[Double]= new Array[Double](FrmSz)
    var Z: Array[Double]= new Array[Double](FrmSz)
  }


  def main(args: Array[String]): Unit = {

    val userName= args(0);
    val password= args(1);
    val hostName= args(2);

    val RecvQueue= args(3)
    val SendQueue= args(4)

    BffSz = args(5).toInt
    FrmSz = args(6).toInt
    NmbCpCoe = args(7).toInt
    Thrd1=args(8).toDouble
    Thrd2= args(9).toDouble

    //Sending channel
    var connSnd=Connect(userName,password,hostName)
    val chnlSnd = connSnd.createChannel
    chnlSnd.queueDeclare(SendQueue, true, false, false, null)

    // Receiving channel
    var connRcv=Connect(userName,password,hostName)
    val chnlRcv = connRcv.createChannel
    chnlRcv.queueDeclare(RecvQueue, true, false, false, null)
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C")

    val consumer = new DefaultConsumer(chnlRcv) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        val message = new String(body, "UTF-8")
        println("Received: " + message)

        val SeqID=getFieldInt("SeqID",message)

        // Get data from packet
        var Pckt= new SensorRowData()
        Pckt.X=getFieldVector("X",message)
        Pckt.Y=getFieldVector("Y",message)
        Pckt.Z=getFieldVector("Z",message)

        // Processing
        val Alert=Process(Pckt)

        // Send to receiver
        var SndMssg:String="{"
        SndMssg+="machineID: Mach1,"
        SndMssg+="sensorID: Sense1,"
        SndMssg+="SeqID: " + SeqID.toString + ","
        SndMssg+="Alert: " + Alert.toString + ","
        SndMssg += "\"X\":" + Frm2Str(Pckt.X).replace(","," ") + ",";
        SndMssg += "\"Y\":" + Frm2Str(Pckt.Y).replace(","," ") + ",";
        SndMssg += "\"Z\":" + Frm2Str(Pckt.Z).replace(","," ") + ",";
        SndMssg+="}"

        chnlSnd.basicPublish("", SendQueue, null, SndMssg.getBytes)
        println("Sent: " + SndMssg )

      }
    }

    chnlRcv.basicConsume(RecvQueue, true, consumer)

    //Send into frames
    //channel1.close
    //conn1.close
  }
}