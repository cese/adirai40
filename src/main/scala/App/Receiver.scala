package App

import java.time.ZonedDateTime
import java.util.concurrent.LinkedBlockingQueue
import scala.collection.mutable.Queue

import java.time.ZonedDateTime
import DevelopedFunctions._
import com.rabbitmq.client._
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Channel
import java.util
import App.Detector.{SensorRowData, getFieldInt, getFieldVector}
import scala.collection.mutable.Queue
import java.util.concurrent.{BlockingQueue, LinkedBlockingQueue}



object Receiver{

  var bufferSz: Int = 20000
  val figw: JPlotAlarmInterface = new JPlotAlarmInterface("",bufferSz)
  val yy: Array[Array[Double]] = new Array[Array[Double]](3)
  yy(0)=Array.fill[Double](bufferSz)(0.0)
  yy(1)=Array.fill[Double](bufferSz)(0.0)
  yy(2)=Array.fill[Double](bufferSz)(0.0)
  var queue = Queue[SensorRowData]()
  var queueblck = new LinkedBlockingQueue[SensorRowData]()

  //Interface features
  figw.grid("on", "on") // grid on;
  figw.font("Arial", 18) // Set font
  figw.ylim(0.1, 4 - 0.1)
  figw.setY_Unit(1)
  figw.xlim(0,bufferSz)
  figw.ylabel("Axis X            Axis Y              Axis Z")
  figw.xlabel("Samples")
  figw.FindColor( 1.0f,bufferSz)

  var a = 10
  var av = 0.0
  var avprev = 0.0
  var pt = -10.0
  var blink = 0
  var counter = 0
  var counter1 = 0
  var FrmSz=64
  var FrmStp=64
  var flag=true

  // Data package format
  class SensorRowData{
    var timestamp:ZonedDateTime=_
    var machineId:String="_"
    var sensorId: String="_"
    var Alert: Int=0
    var X: Array[Double]= new Array[Double](FrmSz)
    var Y: Array[Double]= new Array[Double](FrmSz)
    var Z: Array[Double]= new Array[Double](FrmSz)
  }

  // Plotting thread class
  class PlotThread extends Runnable{
    override def run() {
      while (true) {
        plotAlarmInterfaceStep1()
        plotAlarmInterfaceStep2()
        Thread.sleep(5)
        //println("PlotThread")
      }
    }
  }

  // Alarm thread class
  class BlinkThread extends Runnable {
    override def run(): Unit = {
      var state = 0

      while(true) {
        while (av == 1) {
          if (state == 0) {
            figw.setState(3); state = 1
          }
          else {
            figw.setState(0); state = 0
          }
          Thread.sleep(1000)
        }
        figw.setState(1)
        Thread.sleep(100)
      }
    }
  }

  // Read data from queue to the plot buffer - fase 1
  def plotAlarmInterfaceStep1(): Unit = {

    if (queueblck.size==0)
      return


    val Signal: SensorRowData = queueblck.take()

    //Concatenate signal frames
    yy(0) = yy(0) ++ Signal.X.slice(FrmSz - FrmStp, FrmSz)
    yy(1) = yy(1) ++ Signal.Y.slice(FrmSz - FrmStp, FrmSz)
    yy(2) = yy(2) ++ Signal.Z.slice(FrmSz - FrmStp, FrmSz)

    yy(0) = yy(0).slice(FrmStp, yy(0).length);
    yy(1) = yy(1).slice(FrmStp, yy(1).length);
    yy(2) = yy(2).slice(FrmStp, yy(2).length)

    avprev = av
    av = Signal.Alert
  }

  // Plot the plot buffer fase 1
  def plotAlarmInterfaceStep2(): Unit = {

    counter += 1
    counter1 += 1

    if (counter1 == 10 ) {   // updates in every 10 function evokations

      //Update signal plots
      figw.wiggle(yy, Array(-10.0, -10.0))

      // Update gauges
      val CF = figw.meanCrestFactor(yy)
      val ClF = figw.meanClearanceFactor(yy)
      val IF = figw.meanImpulseFactor(yy)
      val SF = figw.meanShapeFactor(yy)

      figw.updateGaugesValue(CF, ClF, IF, SF)
      figw.updateDisplayValues(CF, ClF, IF, SF)

      counter1 = 0
    }
  }

  // Connect to RabbitMQ
  def Connect(userName:String,password:String,hostName:String):Connection ={
    val factory = new ConnectionFactory();
    val userName= "inesc";
    val password= "adira_1n35c";
    val hostName= "194.117.30.45";
    val portNumber = 5672;
    factory.setUsername(userName)
    factory.setPassword(password)
    factory.setHost(hostName)
    factory.setPort(portNumber)
    val connection = factory.newConnection()
    connection
  }

  var AlarmBlink = new Thread(new BlinkThread())


  def main(args: Array[String]): Unit = {

    val userName= args(0);
    val password= args(1);
    val hostName= args(2);
    val RcvQueue= args(3);

    FrmSz=args(4).toInt; FrmStp=args(4).toInt

    // Active user interface thread
    var th = new Thread(new PlotThread())
    th.setName("PLOT"); th.start()

    AlarmBlink.start()

    // Receiving connection
    var conn1=Connect(userName,password,hostName)
    val channel1 = conn1.createChannel
    channel1.queueDeclare(RcvQueue, true, false, false, null)
    println("Waiting for messages. To exit press CTRL+C")

    val consumer = new DefaultConsumer(channel1) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        val message = new String(body, "UTF-8")
        println("Received: " + message)

        //Read packet sequence number
        val SeqID=getFieldInt("SeqID",message)

        // Extract data from packet
        var Pckt= new SensorRowData()
        Pckt.X=getFieldVector("X",message)
        Pckt.Y=getFieldVector("Y",message)
        Pckt.Z=getFieldVector("Z",message)
        Pckt.Alert= getFieldInt("Alert",message)

        queueblck.add(Pckt)

      }
    }

    channel1.basicConsume(RcvQueue, true, consumer)
  }
}