package App


import java.io.{FileNotFoundException, PrintWriter}
import DevelopedFunctions._;
//import pt.inescn.plot._
//import pt.inescn.utils._
//import pt.inescn.features.LFCC

/**
  *   root/runMain pt.inescn.detector.stream.AnomalyDetectorTest
  */
object AnomalyDetectorTest{

  //=======================================================================================
  def main(args: Array[String]): Unit = {

    // variaveis constantes
    //val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/DB_SmDnCmDn/Rol1_1000.csv"
    val Path= "./data/inegi/anomaly_detection_data/rol1_rpm1000_hp0_b_mm0_exp1.csv"
    val FrmSz=256
    val FrmStp=128
    val NmbCpCoe=5
    val BffSz=64

    // Reads the data set
    //---------------------
    val rdf = new Read_Data_File(Path, 1, 1)
    val Sgnl= new Array[Double](rdf.Table.size())
    for ( i <- 0 until rdf.Table.size() )
      Sgnl(i)=rdf.Table.get(i)(3)

    //Windowing
    val frm= new Frmng().FrmngFunc(Sgnl,FrmSz,FrmStp, Array.fill[Double](FrmSz)(1) )

    //LFCC initialization
    val LFCCclss = LFCC(5100,5,2550,10,5,FrmSz)
    val AnomalyDetectorClss = AnomalyDetector(5,0.9,0.9)
    var Buffer = List[Array[Double]]()

    val aBuffer = new Array[Double](frm.length)
    val iBuffer = new Array[Double](frm.length)

    val fig = new JPlot("h")
    fig.grid("on", "on") // grid on;


    for( i<-0 until frm.length) {

      println(s"\nFrame $i---------------------------")
      //LFCC computation
      val lfcc = LFCCclss.LFCCcomp(frm(i))

      if (Buffer.size < BffSz)
        Buffer ++= List(lfcc)
      else{
        Buffer.drop(0)
        Buffer ++= List(lfcc)

        //Smoothing
        val smthLfcc= new Array[Double](NmbCpCoe)
        val tmp = new Array[Double](BffSz)
        for (k <- 0 until NmbCpCoe){
          for(j <- 0 until BffSz)
            tmp(j) = Buffer(j)(k)
          smthLfcc(k)=AnomalyDetectorClss.median(tmp)
        }
        print(s"LFCC ")
        println(smthLfcc.mkString(";"))

        // Anomaly detection(Prediction)
        val FD = AnomalyDetectorClss.predict(smthLfcc)
        AnomalyDetectorClss.train(smthLfcc)

        aBuffer(i)= FD.toDouble;iBuffer(i)= i.toDouble

        println(s"Prediction $FD")
      }
    }
    fig.plot(iBuffer,aBuffer,"-r",1.0f,"AAPL")
    println(aBuffer.mkString(";"))

  }
}


case class ItrtvObsrvr(Sz:Int){
  val sum2 = Array.fill[Double](Sz)(0.0)
  val sum = Array.fill[Double](Sz)(0.0)
  val mu = Array.fill[Double](Sz)(0.0)
  val std = Array.fill[Double](Sz)(0.0)
  var N = 0
  val alpha = 1

  def update(v:Array[Double]): Unit ={
    for (i <- 0 until Sz){
      sum2(i)=alpha*sum2(i)+Math.pow(v(i),2)
      sum(i)=alpha*sum(i)+v(i)
      mu(i)=sum(i)/N
      std(i)= Math.sqrt(Math.abs(sum2(i)/N-Math.pow(mu(i),2)))
      N+=1
    }
  }
}



case class AnomalyDetector(FtrsNmbr:Int, AnomThres1:Double, AnomThres2:Double ) {

  val ItObs= new ItrtvObsrvr(FtrsNmbr)  //iteraction observer
  var b=0.0


  // Files for debug porposes
  var pwprob: PrintWriter = null
  try
    pwprob = new PrintWriter("./prob.csv")
  catch {
    case e: FileNotFoundException =>
      e.printStackTrace()
  }

  var pwdecision: PrintWriter = null
  try
    pwdecision = new PrintWriter("./decision.csv")
  catch {
    case e: FileNotFoundException =>
      e.printStackTrace()
  }



  def trainExtModel(model:Array[Array[Double]],example:Array[Double]): Array[Array[Double]] ={

    for (i <- 0 until FtrsNmbr){
      model(0)(i)=ItObs.alpha*model(0)(i)+example(i)
      model(1)(i)=ItObs.alpha*model(1)(i)+Math.pow(example(i),2)
      model(2)(i)+= 1
    }
    model
  }

  // Unsupervised training
  def train(v:Array[Double]): Array[Array[Double]] = {
    ItObs.update(v)
    var out1 = new Array[Array[Double]](2)

    out1(0) = ItObs.mu
    out1(1) = ItObs.std
    out1
  }

  def predictExtModel( model:Array[Array[Double]],example:Array[Double] ): Int = {

    var out = new Array[Double](FtrsNmbr)

    for (i <- 0 until FtrsNmbr) {

      val mu=model(0)(i)/model(2)(i)
      var std = Math.sqrt(Math.abs(model(1)(i)/model(2)(i)-Math.pow(mu,2)))

      if (std == 0)
        std = 10e-9
      var prob = 2 * Math.pow(std, 2) / (Math.pow(std, 2) + Math.pow(example(i) - mu, 2))

      if (prob > 1)
        prob = 1

      out(i) = 1 - prob
    }


    var A = Array.fill[Double](FtrsNmbr)(0)
    for (i <- 0 until FtrsNmbr)
      if (out(i) > AnomThres1)
        A(i) = 1

    b = out.sum / out.length

    var a = Array.fill[Double](FtrsNmbr)(0)
    for (i <- 0 until FtrsNmbr)
      if (A(i) > AnomThres2)
        a(i) = 1

    var FinalDecision = 0

    if (a.sum > 0) {
      FinalDecision = 1
    }else{
      FinalDecision = 0
    }

    FinalDecision
  }



  def predict( v:Array[Double] ): Int = {

    //print(s"Mu: "); for(d <- ItObs.mu){print("%.2f ".format(d))};println()
    //print(s"Sigma: "); for(d <- ItObs.std){print("%.2f ".format(d))};println()

    // Cantelli test
    var out = new Array[Double](FtrsNmbr)
    for (i <- 0 until FtrsNmbr) {
      if (ItObs.std(i) == 0)
        ItObs.std(i) = 10e-9
      var prob = 2 * Math.pow(ItObs.std(i), 2) / (Math.pow(ItObs.std(i), 2) + Math.pow(v(i) - ItObs.mu(i), 2))

      if (prob > 1)
        prob = 1

      out(i) = 1 - prob
    }

    // Write to a file for debub purposes
    //for(d <- out){pwprob.write("%.3f;".format(d).replace(',','.'))}; pwprob.write("\n")


    // Probabilities threshold
    var A = Array.fill[Double](FtrsNmbr)(0)
    for (i <- 0 until FtrsNmbr)
      if (out(i) > AnomThres1)
        A(i) = 1

    // Write to a file for debub purposes
    //for(d <- A){pwdecision.write("%.3f;".format(d).replace(',','.'))}; pwdecision.write("\n")

    //Percentage threshold of anomalous variables
    b = out.sum / out.length
    var a = Array.fill[Double](FtrsNmbr)(0)
    for (i <- 0 until FtrsNmbr)
      if (A(i) > AnomThres2)
        a(i) = 1

    var FinalDecision = 0

    if (a.sum > 1) {
      FinalDecision = 1
    }else{
      FinalDecision = 0
    }

    FinalDecision
  }



  def median(Values: Array[Double]): Double = {
    val sortedArray = Values.sorted
    var v=0.0

    if (sortedArray.length % 2 == 1) {
      v = sortedArray(sortedArray.size/2)
    }
    else {
      val (up, down) = sortedArray.splitAt(sortedArray.size / 2)
      v=(up.last + down.head) / 2
    }
    v
  }

}
