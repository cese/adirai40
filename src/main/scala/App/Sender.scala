package App;
import DevelopedFunctions._;
import com.rabbitmq.client._;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Channel;

object Sender {

  // Connect to RabbtiMQ
  def Connect(userName:String,password:String,hostName:String):Connection ={
    val factory = new ConnectionFactory();
    val userName= "inesc";
    val password= "adira_1n35c";
    val hostName= "194.117.30.45";
    val portNumber = 5672;
    factory.setUsername(userName)
    factory.setPassword(password)
    factory.setHost(hostName)
    factory.setPort(portNumber)
    val connection = factory.newConnection()
    connection
  }

  //Convert an array into a string for JSON format
  def Frm2Str(frm:Array[Double]): String ={
    var  frmstr:String=""
    for (i <- 0 to frm.length-1)
        frmstr += frm(i).toString + ","
    frmstr += frm(frm.length-1).toString
    frmstr
  }


  def main(args: Array[String]): Unit = {

    val userName= args(0);
    val password= args(1);
    val hostName= args(2);

    val SendQueue = args(3)
    val FrmSz = args(4).toInt;  val FrmStp = args(4).toInt

    // Get current directory
    val currentDirectory = new java.io.File(".").getCanonicalPath
    println(currentDirectory)

    //Open the data file
    val Path = currentDirectory + args(5)
    val rdf = new Read_Data_File(Path, 1, 1)
    val SgnlX = new Array[Double](rdf.Table.size())
    val SgnlY = new Array[Double](rdf.Table.size())
    val SgnlZ = new Array[Double](rdf.Table.size())

    for ( i <- 0 to rdf.Table.size()-1 ) {
      SgnlX(i) = rdf.Table.get(i)(1)
      SgnlY(i) = rdf.Table.get(i)(2)
      SgnlZ(i) = rdf.Table.get(i)(3)
    }

    // Signal framing/windowing
    val frmX = new Frmng().FrmngFunc(SgnlX, FrmSz, FrmStp, Array.fill[Double](FrmSz)(1))
    val frmY = new Frmng().FrmngFunc(SgnlY, FrmSz, FrmStp, Array.fill[Double](FrmSz)(1))
    val frmZ = new Frmng().FrmngFunc(SgnlZ, FrmSz, FrmStp, Array.fill[Double](FrmSz)(1))

    //Open RabbitMQ connection
    var conn = Connect(userName,password,hostName)
    System.out.println(conn.getPort)
    System.out.println(conn.getAddress)
    val channel = conn.createChannel()

    channel.queueDeclare(SendQueue, true, false, false, null)


    // Permanent cycle of signal frames sending
    var SeqID=0
    var i=0
    while (true){

      var k=(i % frmX.length)

      // Create JSON message
      var SndMssg:String="{"
      SndMssg+="machineID: Mach1,"
      SndMssg+="sensorID: Sense1,"
      SndMssg+="SeqID: " + SeqID.toString + ","
      SndMssg += "\"X\":" + Frm2Str(frmX(k)).replace(","," ") + ",";
      SndMssg += "\"Y\":" + Frm2Str(frmY(k)).replace(","," ") + ",";
      SndMssg += "\"Z\":" + Frm2Str(frmZ(k)).replace(","," ") + ",";
      SndMssg+="}"

      // Send message
      channel.basicPublish("", SendQueue, null, SndMssg.getBytes)
      println("Sent: " + SndMssg)

      // Flow control
      Thread.sleep(args(6).toInt)   // FrmSz/5000

      SeqID+=1
      i+=1
    }

    channel.close;  conn.close

  }
}
