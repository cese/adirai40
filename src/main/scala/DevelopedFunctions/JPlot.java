package DevelopedFunctions;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RefineryUtilities;
import org.jfree.util.ShapeUtilities;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//import com.orsonpdf.PDFDocument;
//import com.orsonpdf.PDFGraphics2D;
//import com.orsonpdf.Page;
//import org.jfree.ui.Spacer;

public class JPlot extends ApplicationFrame {

    XYSeriesCollection dataset;
    ArrayList<Color> colors;
    ArrayList<Stroke> strokes;
    ArrayList<Shape> shapes;
    JFreeChart chart;
    LegendTitle legend;
    Font font;
    int serieskey=0;
    boolean holdon=false;


    public JPlot(final String title) {
        super(title);
        dataset = new XYSeriesCollection();
        colors = new ArrayList<Color>();
        strokes = new ArrayList<Stroke>();
        shapes = new ArrayList<Shape>();
        chart = createChart(dataset,title);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(1000, 540));
        setContentPane(chartPanel);
    }

    public void updatePlot(double[] x, double[] y, int serieIndex){
        for (int i = 0; i < x.length; i++)
            dataset.getSeries(serieIndex).update(x[i],y[i]);
    }


    public void plot( double[] y){

        this.grid("on","on");                 // grid on;
        this.font("Arial",12);             // Set font

        double min=y[0],max=y[0];
        double [] x= new double[y.length];
        for(int i=0; i<y.length; i++) {
            x[i]=i;
            if (y[i] < min)
                min = y[i];
            if (y[i] > max)
                max = y[i];
        }

        if( min==max) {
            min = min - 1;
            max = max + 1;
        }

        this.ylim(min,max);
        this.ylabel("y");
        this.xlabel("x");
        this.holdon();

        this.plot(x,y,"-k",(float)1.0,"");

    }


    public void plot(double[] x, double[] y, String spec, float lineWidth, String title) {
        serieskey++;
        final XYSeries series = new XYSeries(serieskey);

        if (!holdon){
            dataset.removeAllSeries();
        }

        // if x is index vector
        if (x.length!=y.length){
            x = new double[y.length];
            for(int i=1; i<=x.length ; i++)
                x[i-1]=i;
        }
        for (int i = 0; i < x.length; i++)
            series.add(x[i],y[i]);
        dataset.addSeries(series);

        // Line style
        FindColor(spec,lineWidth);

        // Add customization options to chart
        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for (int i = 0; i < colors.size(); i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, true);
            renderer.setSeriesStroke(i, strokes.get(i));
            renderer.setSeriesPaint(i, colors.get(i));
            renderer.setSeriesShape(i, shapes.get(i));
        }
        plot.setRenderer(renderer);

        // Opens plot window
        //this.pack();
        //RefineryUtilities.centerFrameOnScreen(this);
        //this.setVisible(true);
    }

    public void figure(){
        this.pack();
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
    }

    public void close(){
        //this.close();
        //setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setVisible(false);
    }




    private JFreeChart createChart(final XYDataset dataset, String title) {

        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
                title,      // chart title
                "X",                      // x axis label
                "Y",                      // y axis label
                dataset,                  // data
                PlotOrientation.VERTICAL,
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
        );

        // Add customization options to chart
        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for (int i = 0; i < colors.size(); i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, true);
        }
        plot.setRenderer(renderer);


        ((NumberAxis)plot.getDomainAxis()).setAutoRangeIncludesZero(false);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        plot.setBackgroundPaint(Color.WHITE);
        legend = chart.getLegend();
        chart.removeLegend();
        this.chart = chart;

        return chart;

    }

    public void FindColor(String spec, float lineWidth) {
        float dash[] = {5.0f};
        float dot[] = {lineWidth};


        Stroke stroke = new BasicStroke(lineWidth); // Default stroke is line
        if (spec.contains("-"))
            stroke = new BasicStroke(lineWidth);
        else if (spec.contains(":"))
            stroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
        else if (spec.contains("."))
            stroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 2.0f, dot, 0.0f);
        //else if (spec.contains("*"))
            //stroke = null

        strokes.add(stroke);

        Color color = Color.RED; // Default color is red
        if (spec.contains("y"))
            color = Color.YELLOW;
        else if (spec.contains("m"))
            color = Color.MAGENTA;
        else if (spec.contains("c"))
            color = Color.CYAN;
        else if (spec.contains("r"))
            color = Color.RED;
        else if (spec.contains("g"))
            color = Color.GREEN;
        else if (spec.contains("b"))
            color = Color.BLUE;
        else if (spec.contains("k"))
            color = Color.BLACK;
        colors.add(color);


        Shape shape = new Rectangle(0,0); // Default none
        if ( spec.contains("Q") )
            shape = new Rectangle2D.Double(-2.0, -2.0, 4.0, 4.0);
        else if (spec.contains("C"))
            shape = new Ellipse2D.Double(-2.5,-2.5,5,5);
        else if (spec.contains("D"))
            shape =ShapeUtilities.createDiamond(5);
        else if (spec.contains("L"))
            shape = new Rectangle2D.Double(-0.25, -4.0, 0.5, 8.0);
        //Shape shape = ShapeUtilities.createDiagonalCross(3, 1);
        //Shape shape = new Rectangle2D.Double(-3.0, -3.0, 6.0, 6.0);

        shapes.add(shape);

        //markers

    }


    public void CheckExists() {
        if (chart == null) {
            throw new IllegalArgumentException("First plot something in the chart before you modify it.");
        }
    }


    public void grid(String xAxis, String yAxis) {
        CheckExists();
        if (xAxis.equalsIgnoreCase("on")){
            chart.getXYPlot().setDomainGridlinesVisible(true);
            chart.getXYPlot().setDomainMinorGridlinesVisible(true);
            chart.getXYPlot().setDomainGridlinePaint(Color.GRAY);
        } else {
            chart.getXYPlot().setDomainGridlinesVisible(false);
            chart.getXYPlot().setDomainMinorGridlinesVisible(false);
        }

        if (yAxis.equalsIgnoreCase("on")){
            chart.getXYPlot().setRangeGridlinesVisible(true);
            chart.getXYPlot().setRangeMinorGridlinesVisible(true);
            chart.getXYPlot().setRangeGridlinePaint(Color.GRAY);
        } else {
            chart.getXYPlot().setRangeGridlinesVisible(false);
            chart.getXYPlot().setRangeMinorGridlinesVisible(false);
        }
    }


    public void font(String name, int fontSize) {
        CheckExists();
        font = new Font(name, Font.PLAIN, fontSize);
        chart.getTitle().setFont(font);
        chart.getXYPlot().getDomainAxis().setLabelFont(font);
        chart.getXYPlot().getDomainAxis().setTickLabelFont(font);
        chart.getXYPlot().getRangeAxis().setLabelFont(font);
        chart.getXYPlot().getRangeAxis().setTickLabelFont(font);
        legend.setItemFont(font);
    }

    public void title(String title) { CheckExists(); chart.setTitle(title); }
    public void xlim(double l, double u) { CheckExists(); chart.getXYPlot().getDomainAxis().setRange(l, u); }
    public void ylim(double l, double u) { CheckExists(); chart.getXYPlot().getRangeAxis().setRange(l, u); }
    public void xlabel(String label) { CheckExists();chart.getXYPlot().getDomainAxis().setLabel(label); }
    public void ylabel(String label) { CheckExists();chart.getXYPlot().getRangeAxis().setLabel(label); }
    public void clear () { dataset.removeAllSeries();}
    public void holdon() { holdon=true;}
    public void holdoff(){ holdon=false;}

    // Novo codigo
    //==================================================================================================================
    public void setX_visible(boolean flag) { chart.getXYPlot().getDomainAxis().setVisible(flag); }
    public void setY_visible(boolean flag) { chart.getXYPlot().getRangeAxis().setVisible(flag); }
    public void text(String label, double x, double y, int fsize, String font  ) {

        /*final XYPointerAnnotation pointer = new XYPointerAnnotation(label, 1, 0, 3.0 * Math.PI / 4.0);
        pointer.setBaseRadius(35.0);
        pointer.setTipRadius(10.0);
        pointer.setFont(new Font("SansSerif", Font.PLAIN, 9));
        pointer.setPaint(Color.blue);
        //pointer.setTextAnchor(TextAnchor.HALF_ASCENT_RIGHT);
        pointer.setOutlineVisible(true);
        chart.getXYPlot().addAnnotation(pointer);*/


        final XYTextAnnotation text= new XYTextAnnotation(label,x,y);
        text.setFont(new Font(font, Font.BOLD, fsize ));
        //text.draw(1,1,);
        chart.getXYPlot().addAnnotation(text);

    }

    public void setY_Unit(double unit){
        TickUnits units = new TickUnits();
        units.add(new NumberTickUnit(unit));
        chart.getXYPlot().getRangeAxis().setStandardTickUnits(units);
    }


    //==================================================================================================================

    public void legend(String position) {
        CheckExists();
        legend.setItemFont(font);
        legend.setBackgroundPaint(Color.WHITE);
        legend.setFrame(new BlockBorder(Color.BLACK));
        if (position.toLowerCase().equals("northoutside")) {
            legend.setPosition(RectangleEdge.TOP);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("eastoutside")) {
            legend.setPosition(RectangleEdge.RIGHT);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("southoutside")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("westoutside")) {
            legend.setPosition(RectangleEdge.LEFT);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("north")) {
            legend.setPosition(RectangleEdge.TOP);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.50,0.98,legend, RectangleAnchor.TOP);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("northeast")) {
            legend.setPosition(RectangleEdge.TOP);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.98,0.98,legend, RectangleAnchor.TOP_RIGHT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("east")) {
            legend.setPosition(RectangleEdge.RIGHT);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.98,0.50,legend, RectangleAnchor.RIGHT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("southeast")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.98,0.02,legend, RectangleAnchor.BOTTOM_RIGHT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("south")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.50,0.02,legend, RectangleAnchor.BOTTOM);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("southwest")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.02,0.02,legend, RectangleAnchor.BOTTOM_LEFT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("west")) {
            legend.setPosition(RectangleEdge.LEFT);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.02,0.50,legend, RectangleAnchor.LEFT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("northwest")) {
            legend.setPosition(RectangleEdge.TOP);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.02,0.98,legend, RectangleAnchor.TOP_LEFT);
            chart.getXYPlot().addAnnotation(ta);
        }
    }

    public void saveas(String fileName, int width, int height) {
        CheckExists();
        File file = new File(fileName);
        try {
            //ChartUtilities.saveChartAsJPEG(file,this.chart,width,height);
            ChartUtilities.saveChartAsPNG(file,this.chart,width,height);
            //writeAsPDF(this.chart);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // Write to PDF
    /*public void writeAsPDF( JFreeChart chart, OutputStream out, int width, int height )
    {
        try
        {
            Rectangle pagesize = new Rectangle( width, height );
            Document document = new Document( pagesize, 50, 50, 50, 50 );
            PdfWriter writer = PdfWriter.getInstance( document, out );
            document.open();
            PdfContentByte cb = writer.getDirectContent();
            PdfTemplate tp = cb.createTemplate( width, height );
            Graphics2D g2 = tp.createGraphics( width, height, new DefaultFontMapper() );
            Rectangle2D r2D = new Rectangle2D.Double(0, 0, width, height );
            chart.draw(g2, r2D);
            g2.dispose();
            cb.addTemplate(tp, 0, 0);
            document.close();
        }
        catch (Exception e)
        {
            LOG.error( e );
        }
    }*/


    /*public void writeAsPDF(JFreeChart chart) throws IOException {
        //JFreeChart chart = createChart(createDataset());
        PDFDocument pdfDoc = new PDFDocument();
        pdfDoc.setTitle("PDFBarChartDemo1");
        pdfDoc.setAuthor("Object Refinery Limited");
        Page page = pdfDoc.createPage(new Rectangle(612, 468));
        PDFGraphics2D g2 = page.getGraphics2D();
        chart.draw(g2, new Rectangle(0, 0, 612, 468));
        pdfDoc.writeToFile(new File("C:/Users/RSousa1/Desktop/PDFBarChartDemo1.pdf"));
    }*/


    public void wiggle(double [] x, double [][] yy){

        this.grid("on","on");                 // grid on;
        this.font("Arial",12);             // Set font
        this.ylim(0.1,yy.length+1-0.1);
        this.setY_Unit(1);
        this.ylabel("COLUMNS");
        this.xlabel("INSTANCES");
        this.holdon();

        double [] yfinal= new double[yy[0].length];

        this.clear();

        for(int j=0; j < yy.length ; j++){

            //compute mean and max
            double cum=0;
            for(int k=0 ; k < yy[0].length ; k++)
                cum += yy[j][k];

            for(int k=0; k < yy[0].length; k++ )
                yfinal[k]= yy[j][k] - cum/yy[0].length ;


            double max=0;
            for(int k=0; k <yy[0].length; k++)
                if (Math.abs(yfinal[k]) > max)
                    max = Math.abs(yfinal[k]);

            for(int k=0; k < yy[0].length; k++ )
                if (max!=0)
                    yfinal[k] = yfinal[k] / max*0.45 + j + 1;
                else
                    yfinal[k] = j + 1;



            this.plot(x, yfinal ,"-k", 1.0f, "h");
        }
    }



    public static void main(final String[] args) {

        // Create some sample data
        double [] ay1 = new double[1];
        double [] ay2 = new double[1];
        List<Double> y1 = new LinkedList<Double>();
        List<Double> y2 = new LinkedList<Double>();

        final JPlot fig = new JPlot("h");
        fig.grid("on","on");                 // grid on;
        fig.font("Arial",12);             // Set font
        fig.ylim(-10,10);
        fig.holdon();

        int bufferSz=100;
        double[] x = new double[bufferSz];
        fig.xlim(0,bufferSz);

        final JPlot figw = new JPlot("");
        double [][] yy = new double[2][];

        figw.figure();
        fig.figure();
        // Use in real-time
        //---------------------------------------------------------------------
        for(int i = 1; i < 20000; i++){

            if ( i < bufferSz ) {
                x[i] = i;
                y1.add( Math.random() * 10 - 0 );
                y2.add( Math.random() * 10 - 10);
            }
            else {
                y1.remove( 0 );
                y1.add( Math.random() * 10 - 0 );
                y2.remove( 0 );
                y2.add( Math.random() * 10 - 10 );
            }

            if( (i % 1)==0 ){

                ay1 =  new double[y1.size()];
                ay2 =  new double[y2.size()];

                for(int j=0; j < y1.size() ; j++ )
                    ay1[j]=y1.get(j);

                for(int j=0; j < y2.size() ; j++ )
                    ay2[j]=y2.get(j);

                yy[0]=ay1;
                yy[1]=ay2;

                //fig.clear();
                //fig.plot(x, ay1 , "Dr", 2.0f, "AAPL");
                //fig.plot(x, ay2, "-k", 3.0f, "BAC");  // plot(x,y2,':k','LineWidth',3);
                figw.wiggle(x,yy);

                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }


        //Wiggle  test
        //-----------------------------------------------------------------------------
        /*int Sz=1000;
        double [] x1 = new double[Sz];

        for(int k=0; k < yy.length; k++)
            yy[k]= new double[Sz];

        for(int k=0; k < Sz; k++){
            x1[k]=k;
        }

        for(int m=0; m<3; m++) {
            for (int k = 0; k < Sz; k++) {
                yy[m][k] = Math.random() * 10 - 0;
            }
        }*/

        //final JPlot figw = new JPlot("");

    }

}