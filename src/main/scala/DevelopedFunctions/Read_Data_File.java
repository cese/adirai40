package DevelopedFunctions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Read_Data_File {

    public List<double[]> Table;
    public String[] Hdr;
    public double[] Exmpl;


    public Read_Data_File(String Path, int ntrck, int dttp) {

        String type = new String();
        int idx;

        //Finds the file format
        // -----------------------------------------------------
        idx = Path.indexOf(".");
        if (idx != -1)
            type = "csv";
        else
            type = Path.substring(idx);


        System.out.print("Reading a "+ type + " file\n");

        switch (type) {
            case "csv":
                read_csv(Path);
                break;
            case "arff":
                read_arff(Path);
                break;
        }
    }

    public void  read_arff(String Path){

        //Open file
        try (BufferedReader br = new BufferedReader(new FileReader(Path))) {
            String line;
            while ((line = br.readLine()) != null) {



                System.out.print(line);
            }
        }
        catch(Exception e) {

        }


        //Line counting
        //-------------------


        // Read the header
        // --------------------------------------------------------------------------


        //Le os atributos e suas caracteristicas

    }


    public void  read_csv(String Path) {

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            //
            br = new BufferedReader(new FileReader(Path));

            Table= new ArrayList<double[]>();

            //Read CSV header
            Hdr = br.readLine().split(cvsSplitBy);
            Exmpl= new double[Hdr.length];

            while ((line = br.readLine()) != null) {
                String[] exmlstr = line.split(cvsSplitBy);
                Exmpl= new double[Hdr.length];
                for(int i=0;i<exmlstr.length; i++)
                    Exmpl[i]= Double.parseDouble(exmlstr[i]);
                Table.add(Exmpl);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public void  read_csv2(String Path) {

        /*String fileName = "data.csv";
        CSVReader reader = new CSVReader(new FileReader(Path));


        // if the first line is the header
        String[] header = reader.readNext();

        // iterate over reader.readNext until it returns null
        String[] line = reader.readNext();*/
    }


}