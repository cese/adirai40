package DevelopedFunctions

// TODO: add comments to class and methods. Explain what is happening. Add references
// TODO: FCC + Cantelli detector ?
case class CantelliFCCAnomalyDetector(FtrsNmbr:Int, AnomThres1:Double, AnomThres2:Double ) {

  // Observer
  // Sz -Number of features
  case class ItrtvObsrvr(Sz:Int){
    val sum2: Array[Double] = Array.fill[Double](Sz)(0.0)
    val sum: Array[Double] = Array.fill[Double](Sz)(0.0)
    val mu: Array[Double] = Array.fill[Double](Sz)(0.0)
    val std: Array[Double] = Array.fill[Double](Sz)(0.0)
    var N = 0.0
    val alpha = 1.0

    def update(v:Array[Double]): Unit ={
      var i=0
      while (i< Sz){
        sum2(i)=alpha*sum2(i)+Math.pow(v(i),2)
        sum(i)=alpha*sum(i)+v(i)
        mu(i)=sum(i)/N
        std(i)= Math.sqrt(Math.abs(sum2(i)/N-Math.pow(mu(i),2)))
        N+=1
        i=i+1
      }
    }
  }

  val ItObs= ItrtvObsrvr(FtrsNmbr)  //interaction observer
  var b=0.0

  // Return new model
  def currentModel(): Array[Array[Double]] = {
    val out1 = new Array[Array[Double]](2)
    out1(0) = ItObs.mu
    out1(1) = ItObs.std
    out1
  }


  // Unsupervised training
  def train(v:Array[Double]): Array[Array[Double]] = {


    ItObs.update(v)
    currentModel()
  }


  def predict( v:Array[Double] ): Int = {

    //print(s"Mu: "); for(d <- ItObs.mu){print("%.2f ".format(d))};println()
    //print(s"Sigma: "); for(d <- ItObs.std){print("%.2f ".format(d))};println()

    // Cantelli test
    val out = new Array[Double](FtrsNmbr)
    var i=0
    while(i<FtrsNmbr){
      if (ItObs.std(i) == 0)
        ItObs.std(i) = 10e-9
      var prob = 2 * Math.pow(ItObs.std(i), 2) / (Math.pow(ItObs.std(i), 2) + Math.pow(v(i) - ItObs.mu(i), 2))

      if (prob > 1)
        prob = 1

      out(i) = 1 - prob
      i=i+1
    }

    // Write to a file for debub purposes
    //for(d <- out){pwprob.write("%.3f;".format(d).replace(',','.'))}; pwprob.write("\n")

    // Probabilities threshold
    val A = Array.fill[Double](FtrsNmbr)(0)
    i=0
    while(i<FtrsNmbr){
      if (out(i) > AnomThres1)   A(i) = 1
      i=i+1
    }

    // Write to a file for debub purposes
    //for(d <- A){pwdecision.write("%.3f;".format(d).replace(',','.'))}; pwdecision.write("\n")

    //Percentage threshold of anomalous variables
    b = A.sum/A.length

    print("b" + b.toString + "\n")

    val FinalDecision = if (b > AnomThres2){ 1 } else{0 }

    FinalDecision
  }


  // Median of the array Values
  def median(Values: Array[Double]): Double = {
    val sortedArray = Values.sorted
    var v=0.0

    if (sortedArray.length % 2 == 1) {
      v = sortedArray(sortedArray.length/2)
    }
    else {
      val (up, down) = sortedArray.splitAt(sortedArray.length / 2)
      v=(up.last + down.head) / 2
    }
    v
  }

}
